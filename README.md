# Udacity Artificial Intelligence Nanodegree Projects

- Solve a Sudoku with AI
- Build a Game-Playing Agent
- Implement a Planning Search
- Build a Sign Language Recognizer
- Dog Breed Classifier
- Time Series Prediction and Text Generation
- CV Capstone Project: Facial Keypoint Detection